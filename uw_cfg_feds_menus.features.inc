<?php

/**
 * @file
 * uw_cfg_feds_menus.features.inc
 */

/**
 * Implements hook_strongarm_alter().
 */
function uw_cfg_feds_menus_strongarm_alter(&$data) {
  if (isset($data['responsive_menu_combined_settings'])) {
    $data['responsive_menu_combined_settings']->value['devel'] = array(
      'friendly_name' => 'Development',
      'enabled' => 0,
      'weight' => 0,
      'id' => 'devel',
      'pid' => '',
      'depth' => '',
    ); /* WAS: '' */
    $data['responsive_menu_combined_settings']->value['features'] = array(
      'friendly_name' => 'Features',
      'enabled' => 0,
      'weight' => 1,
      'id' => 'features',
      'pid' => '',
      'depth' => '',
    ); /* WAS: '' */
    $data['responsive_menu_combined_settings']->value['menu-audience-menu']['enabled'] = 0; /* WAS: 1 */
    $data['responsive_menu_combined_settings']->value['menu-uw-menu-global-header']['enabled'] = 0; /* WAS: 1 */
  }
}
